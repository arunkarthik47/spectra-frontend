angular.module("app.states", [])
    .constant("States", {
        ROOT: 'root',
        LOGIN: 'loginScreen',
        APP: {
            ROOT: 'app',
            DEVICES: 'app.devices',
            DETAILS: 'app.details',
            VIEW3: 'app.view3'
        },
        SCREENTHREE: 'screenThree'

    });