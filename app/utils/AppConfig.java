package utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class AppConfig {
    public static final String IOT_SERVICE_HOST = "iotservice.api.host";
    public static final String IOT_SERVICE_SCHEME = "iotservice.api.scheme";
    public static final String IOT_SERVICE_PORT = "iotservice.api.port";
    public static final String GATEWAY_SERVICE_HOST = "gatewayservice.api.host";
    public static final String GATEWAY_SERVICE_SCHEME = "gatewayservice.api.scheme";
    public static final String GATEWAY_SERVICE_PORT = "gatewayservice.api.port";
    public static final String AUTH_SERVICE_HOST = "authservice.api.host";
    public static final String AUTH_SERVICE_SCHEME = "authservice.api.scheme";
    public static final String AUTH_SERVICE_PORT = "authservice.api.port";

    // Job Scheduler
    public static final String JOB_SCHEDULER_HOST = "jobscheduler.api.host";
    public static final String JOB_SCHEDULER_SCHEME = "jobscheduler.api.scheme";
    public static final String JOB_SCHEDULER_PORT = "jobscheduler.api.port";

    public static final String MQTT_HOST = "mqtt.host";
    public static final String MQTT_PORT = "mqtt.port";
    public static final String MQTT_PROTOCOL = "mqtt.protocol";
    public static final String MQTT_RETRY_COUNT = "mqtt.retry-count";
    public static final String MQTT_RECONNECT_PERIOD = "mqtt.reconnect-period";
    public static final String APP_VERSION = "app.version";

    public static final String XSRF_TOKEN_COOKIE = "XSRF-TOKEN";
    public static final String AUTH_TOKEN_COOKIE = "auth-token";
    public static final String MQTT_TOKEN_COOKIE = "mqtt-auth-token";

    public static final String CSRF_PROTECTION_ENABLE = "feature-toggles.csrf-protection-enable";
    public static final String TEMPERATURE_GRAPH_AUTO_REFRESH_ENABLE = "feature-toggles.temperature-graph-auto-refresh-enable";

    public static final String ENVIRONMENT = "app.env";

    private Config config;

    @Inject
    public AppConfig() {
        config = ConfigFactory.load();
    }

    private Config config() {
        return config;
    }

    public String getIotServiceScheme() {
        return config().getString(IOT_SERVICE_SCHEME);
    }

    public String getIotServiceHost() {
        return config().getString(IOT_SERVICE_HOST);
    }

    public Integer getIotServicePort() {
        return config().getInt(IOT_SERVICE_PORT);
    }

    public String getGatewayServiceScheme() {
        return config().getString(GATEWAY_SERVICE_SCHEME);
    }

    public String getGatewayServiceHost() {
        return config().getString(GATEWAY_SERVICE_HOST);
    }

    public Integer getGatewayServicePort() {
        return config().getInt(GATEWAY_SERVICE_PORT);
    }

    public String getAuthServiceScheme() {
        return config().getString(AUTH_SERVICE_SCHEME);
    }

    public String getAuthServiceHost() {
        return config().getString(AUTH_SERVICE_HOST);
    }

    public Integer getAuthServicePort() {
        return config().getInt(AUTH_SERVICE_PORT);
    }

    public String getJobSchedulerScheme() {
        return config().getString(JOB_SCHEDULER_SCHEME);
    }

    public String getJobSchedulerHost() {
        return config().getString(JOB_SCHEDULER_HOST);
    }

    public Integer getJobSchedulerPort() {
        return config().getInt(JOB_SCHEDULER_PORT);
    }

    public String getMqttHost() {
        return config().getString(MQTT_HOST);
    }

    public String getMqttPort() {
        return config().getString(MQTT_PORT);
    }

    public String getMqttProtocol() {
        return config().getString(MQTT_PROTOCOL);
    }

    public String getMqttRetryCount() {
        return config().getString(MQTT_RETRY_COUNT);
    }

    public String getMqttReconnectPeriod() {
        return config().getString(MQTT_RECONNECT_PERIOD);
    }

    public String getAppVersion() {
        return config().getString(APP_VERSION);
    }

    public Boolean isCSRFProtectionEnabled() {
        return config().getBoolean(CSRF_PROTECTION_ENABLE);
    }

    public Boolean isTemperatureGraphAutoRefreshEnabled() {
        return config().getBoolean(TEMPERATURE_GRAPH_AUTO_REFRESH_ENABLE);
    }

    public String getEnvironment() {
        return config().getString(ENVIRONMENT);
    }
}
