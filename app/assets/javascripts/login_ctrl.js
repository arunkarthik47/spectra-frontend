angular.module('myApp').controller('loginCtrl', ['$scope', '$http', '$timeout', 'States', '$state', function($scope, $http, $timeout, States, $state) {


    $scope.submit = function() {
        $scope.postLoginData();
    };

    $scope.postLoginData = function() {
        var data = $scope.loginDetails;
        $http.post('/login', data)
            .success(function() {
                $state.go(States.APP.DEVICES);
            })
            .error(function() {});
    };


}]);