package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.libs.ws.WSResponse;

//import utils.Logger;
import utils.AppConfig;
import play.Environment;
import java.net.MalformedURLException;
import java.net.URL;
import play.Logger;

public class NewService implements ApiService {

    //    private static final org.slf4j.Logger LOG = Logger.newLogger();

    @Inject
    private AppConfig appConfig;
    @Inject
    private RestApiClient restApiClient;
    @Inject
    private Environment environment;

    public WSResponse getData() {
        return restApiClient.get("http://ip-api.com/json");
    }

    public WSResponse getDeviceData() {
        return restApiClient.get(getApiServiceUri("/v1/iot_service/devices"));
    }


    public WSResponse getConfig(String macId) {
        return restApiClient.get(getApiServiceUri(String.format("/v1/iot_service/devices/%s/configs", macId)));
    }
    public WSResponse login(JsonNode data) {
        return restApiClient.post(getApiServiceUri("/v1/iot_service/login"), data, null);
    }
    public WSResponse testInstruction(JsonNode data) {
        return restApiClient.post(getApiServiceUri("/v1/iot_service/devices/test"), data, null);
    }
    public WSResponse putData(JsonNode json) {
        return restApiClient.put(getApiServiceUri("/v1/iot_service/devices/frontend"), json);
    }
    @Override
    public String getApiServiceUri(String uri) {
        String urlString = null;
        URL url;
        try {
            /*
               Because of nginx configuration, if front app is running out side of nginx server not able to call
               REST API's using port numbers. So not considering port numbers when we are running in DEV mode.
               TODO: Need to remove check for DEV and code should be common in all environments. Keeping for now.
            */
            if (environment.isDev()) {
                url = new URL(appConfig.getIotServiceScheme(), appConfig.getIotServiceHost(), uri);
            } else {
                url = new URL(appConfig.getIotServiceScheme(), appConfig.getIotServiceHost(),
                        appConfig.getIotServicePort(), uri);
            }
            urlString = url.toString();
            Logger.info("Iot service api : " + urlString);
        } catch (MalformedURLException e) {
            Logger.info("Failed to get iot service api : " + e.getMessage());
        }
        return urlString;
    }

}