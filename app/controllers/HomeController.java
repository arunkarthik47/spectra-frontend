package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;
import com.google.inject.Inject;
import views.html.*;
import play.libs.ws.WSResponse;
import services.NewService;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends RestApiServiceController {
    @Inject
    private  NewService newService;

    @Inject
    private FormFactory formFactory;
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render());
    }

    public Result other(String path) {
        return ok(index.render());
    }
    public Result getData() {
        return getResult(newService.getData());
    }
    public Result getDeviceData() {
        return getResult(newService.getDeviceData());
    }
    public Result getConfig(String macId) {
        return getResult(newService.getConfig(macId));
    }
    public Result authenticate() {
//        DynamicForm formData = formFactory.form().bindFromRequest();
        JsonNode json = request().body().asJson();
        WSResponse wsResponse = newService.login(json);
        return getResult(wsResponse);
    }

    public Result testInstruction() {
        JsonNode json = request().body().asJson();
        WSResponse wsResponse = newService.testInstruction(json);
        return getResult(wsResponse);
    }
    public Result putData() {
        JsonNode json = request().body().asJson();
        return getResult(newService.putData(json));
    }
}
