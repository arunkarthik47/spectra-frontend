angular.module('myApp').controller('mainCtrl', ['$scope', '$http', '$timeout', 'States', '$state', 'MqttClient', 'MqttEventHandler', '$mdSidenav', 'notificationService', function($scope, $http, $timeout, States, $state, MqttClient, MqttEventHandler, $mdSidenav, notificationService) {

    $scope.logout = function() {
        $state.go(States.LOGIN);
    };
    var ctrlName;

    $scope.isConnected = function(status) {

        return status.toString() === "connected";
    };

    $scope.getDeviceDetails = function() {
        $http({
            method: "GET",
            url: "/devices"
        }).then(function mySuccess(response) {
            $scope.devices = response.data;
        }, function myError() {});
    };

    $scope.getDeviceDetails();

    $scope.frontend_service_connected = MqttClient.isConnected();
    MqttEventHandler.subscribe("iot-service/status", ctrlName, function() {
        console.log('test');
        $scope.iot_service_connected = true;
    });


    $scope.uploadFile = function(file, invalidFiles) {
        if (!isValidFile(file, invalidFiles)) {
            return;
        }
        var r = new FileReader();
        r.onload = function(e) {
            var contents = e.target.result;
            var topic;
            //            console.log(contents);
            topic = "iot-service/devices/macId/raspi/firmware";
            angular.forEach($scope.devices, function(value) {
                MqttEventHandler.publish(topic.replace("macId", value.macId), contents, {
                    qos: 2,
                    retain: true
                }, function() {});
            });
        };
        r.readAsText(file);
    };
    var isValidFile = function($file, $invalidFiles) {
        if (!$file) {
            if ($invalidFiles.length > 0) {
                if ($invalidFiles[0].$error === "maxSize")
                    notificationService.showToast("Error: File size cannot be greater than 2MB");
                else
                    notificationService.showToast("Error: " + $invalidFiles[0].$error);
            }
            return false;
        }
        return true;
    };


    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
        return function() {
            $mdSidenav(componentId).toggle();
        };
    }

}]);