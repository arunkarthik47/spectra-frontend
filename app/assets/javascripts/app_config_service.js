(function() {
    "use strict";
    angular.module('myApp').service('AppConfigService', ['$http', '$q', function($http, $q) {

        var env;

        this.getMqttHostConfig = function() {
            return $q(function(resolve, reject) {
                $http({
                        method: 'GET',
                        url: "/mqttConfig"
                    })
                    .success(function(data) {
                        console.log("mqtt config : " + JSON.stringify(data));
                        resolve(data);
                    }).error(function(data) {
                        console.log("failed to get mqtt config : " + data);
                        reject("failed to get mqtt config");
                    });

            });
        };


        this.getAppDetails = function() {
            return $q(function(resolve, reject) {
                $http({
                        method: 'GET',
                        url: "/app-details"
                    })
                    .success(function(data) {
                        console.log("app version : " + JSON.stringify(data));
                        resolve(data);
                    }).error(function(data) {
                        console.log("failed to get app version : " + data);
                        reject("failed to get app version");
                    });

            });
        };

        this.getTemperatureGraphAutoRefreshConfig = function() {
            return $q(function(resolve, reject) {
                $http({
                        method: 'GET',
                        url: "/tempGraphAutoRefreshConfig"
                    })
                    .success(function(data) {
                        resolve(data);
                    }).error(function(data, status) {
                        reject(data, status);
                    });
            });
        };

        this.getEnvironmentDetails = function() {
            return $q(function(resolve, reject) {
                if (env === undefined) {
                    $http({
                            method: 'GET',
                            url: "/environment"
                        })
                        .success(function(data) {
                            console.log("Environment : " + data);
                            env = data;
                            resolve(env);
                        }).error(function(data) {
                            console.log("failed to get environment details : " + data);
                            reject("failed to get environment details");
                        });
                } else {
                    resolve(env);
                }
            });
        };

    }]);
})();