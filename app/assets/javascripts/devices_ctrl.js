angular.module('myApp').controller('devicesCtrl', ['$scope', '$http', '$mdDialog', '$mdMedia', 'Upload', '$timeout', 'MqttEventHandler', '$state', 'States', 'notificationService', function($scope, $http, $mdDialog, $mdMedia, Upload, $timeout, MqttEventHandler, $state, States, notificationService) {


    $scope.selected = [];

    $scope.selected = [];

    var eventHandlers = {};

    $scope.fwProgress = {};
    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };

    var mqttEventHandler;

    var ctrlName = "devicesController";

    $scope.getDeviceDetails = function() {
        $http({
            method: "GET",
            url: "/devices"
        }).then(function mySuccess(response) {
            $scope.devices = response.data;
            $scope.subscribe();
        }, function myError() {});
    };

    $scope.getDeviceDetails();

    $scope.configDialog = function(device) {

        $state.go(States.APP.DETAILS, {
            device: device,
            macId: device.macId
        });
    };

    $scope.uploadFile = function(device, file, invalidFiles) {
        var macId = device.macId;
        if (!isValidFile(file, invalidFiles)) {
            return;
        }
        var r = new FileReader();
        r.onload = function(e) {
            var contents = e.target.result;
            var topic;
            var data = {};
            data.fileName = file.name;
            data.content = contents;
            //            console.log(contents);
            topic = "iot-service/devices/macId/firmware";
            MqttEventHandler.publish(topic.replace("macId", macId), JSON.stringify(data), {
                qos: 2,
                retain: false
            }, function() {});
            $scope.fwProgress[macId] = {};
            $scope.fwProgress[macId].showProgress = true;
            $scope.fwProgress[macId].progress = 0;
        };
        r.readAsText(file);
    };
    var isValidFile = function($file, $invalidFiles) {
        if (!$file) {
            if ($invalidFiles.length > 0) {
                if ($invalidFiles[0].$error === "maxSize")
                    notificationService.showToast("Error: File size cannot be greater than 2MB");
                else
                    notificationService.showToast("Error: " + $invalidFiles[0].$error);
            }
            return false;
        }
        return true;
    };

    $scope.subscribeFwMqttEvent = function(device, callback, macId) {
        var topic;
        topic = "iot-service/devices/macId/firmware/response";
        eventHandlers[macId] = MqttEventHandler.subscribe(topic.replace("macId", macId), ctrlName, callback);
    };

    $scope.getConfigForDevice = function(macId) {
        var config = {
            params: {
                macId: macId
            }
        };
        $http.get('/devices/config', config).then(function mySuccess() {}, function myError() {
            console.log('testing 123');
        });
    };


    var onEventReceived = function(message) {
        message = JSON.parse(message.toString());
        angular.forEach($scope.devices, function(value, index) {
            if (value.macId === message.macId) {
                $scope.devices[index] = message;
            }
        });
    };

    mqttEventHandler = MqttEventHandler.subscribe("iot-service/devices", ctrlName, onEventReceived);

    $scope.subscribe = function() {
        console.log('testing 123');
        angular.forEach($scope.devices, function(value) {
            $scope.subscribeFwMqttEvent(value, function(message) {
                $scope.fwProgress[value.macId] = {};
                if (message) {
                    $scope.fwProgress[value.macId].showProgress = true;
                }
                console.log(message);
                message = message.toString();
                if (message.includes("%")) {
                    var msgLength = message.length;
                    var floatValue = message.substring(0, (msgLength - 1));
                    $scope.fwProgress[value.macId].progress = Math.floor(floatValue).toString();
                    console.log($scope.fwProgress[value.macId].progress);
                    if ($scope.fwProgress[value.macId].progress === "100") {
                        $scope.fwProgress[value.macId].showProgress = false;
                    }
                }
            }, value.macId);
        });

    };
}]);


//  $scope.devices = [{
//        "macId": "40:8d:5c:ae:61:d6",
//        "name": "Spectra-7 Tester",
//        "manufacturedDate": "20170824",
//        "serialNumber": "123456",
//        "productId": "s7tester-4d",
//        "firmware": "v.2017-08-24",
//        "configurations": [{
//            "ConfigurationName": "8150 M Module",
//            "PanelType": "815M",
//            "VddRail": "5.0",
//            "ModuleCurrent": 12000,
//            "RejectionThreshold": "10",
//            "Device1Address": "30",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "03",
//            "Device2Address": "34",
//            "Device2RegAddress": "11",
//            "Device2RegContent": "34",
//            "Active": true,
//            "RetryCount": "2",
//            "FirmwareVersion": "170802"
//        }, {
//            "ConfigurationName": "8150 Q-M-41 Module",
//            "PanelType": "81QM",
//            "VddRail": "5.0",
//            "ModuleCurrent": 4000,
//            "RejectionThreshold": "04",
//            "Device1Address": "30",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "03",
//            "Device2Address": "36",
//            "Device2RegAddress": "11",
//            "Device2RegContent": "36",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "7150 Module",
//            "PanelType": "7150",
//            "VddRail": "5.0",
//            "ModuleCurrent": 5000,
//            "RejectionThreshold": "04",
//            "Device1Address": "4C",
//            "Device1RegAddress": "0F",
//            "Device1RegContent": "C1",
//            "Device2Address": "16",
//            "Device2RegAddress": "01",
//            "Device2RegContent": "23",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "8200 Module",
//            "PanelType": "8200",
//            "VddRail": "3.3",
//            "ModuleCurrent": 5000,
//            "RejectionThreshold": "04",
//            "Device1Address": "36",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "63",
//            "Device2Address": "37",
//            "Device2RegAddress": "11",
//            "Device2RegContent": "37",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "8050 Module",
//            "PanelType": "8050",
//            "VddRail": "5.0",
//            "ModuleCurrent": 5000,
//            "RejectionThreshold": "04",
//            "Device1Address": "35",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "5F",
//            "Device2Address": "FF",
//            "Device2RegAddress": "FF",
//            "Device2RegContent": "FF",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "8150 Module",
//            "PanelType": "8150",
//            "VddRail": "5.0",
//            "ModuleCurrent": 12000,
//            "RejectionThreshold": "04",
//            "Device1Address": "30",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "03",
//            "Device2Address": "34",
//            "Device2RegAddress": "11",
//            "Device2RegContent": "34",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "8050 Q Module",
//            "PanelType": "805Q",
//            "VddRail": "5.0",
//            "ModuleCurrent": 2500,
//            "RejectionThreshold": "04",
//            "Device1Address": "37",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "7F",
//            "Device2Address": "FF",
//            "Device2RegAddress": "FF",
//            "Device2RegContent": "FF",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }, {
//            "ConfigurationName": "8150 Q Module",
//            "PanelType": "815Q",
//            "VddRail": "5.0",
//            "ModuleCurrent": 4000,
//            "RejectionThreshold": "04",
//            "Device1Address": "30",
//            "Device1RegAddress": "11",
//            "Device1RegContent": "03",
//            "Device2Address": "36",
//            "Device2RegAddress": "11",
//            "Device2RegContent": "36",
//            "Active": false,
//            "RetryCount": "2",
//            "FirmwareVersion": "XXXXX2"
//        }]
//    }];
//



//        if (device.configurations.length === 0)
//            return;
//        $mdDialog.show({
//            controller: 'dialogCtrl',
//            templateUrl: 'assets/views/dialog.html',
//            clickOutsideToClose: true,
//            locals: {
//                device: device
//            },
//            fullscreen: $mdMedia('xs')
//        }).then(function() {}, function() {});