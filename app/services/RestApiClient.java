package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.Environment;
import play.Logger;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.Http;

import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static play.mvc.Http.Context.Implicit.request;

public class RestApiClient {

    @Inject
    private WSClient wsClient;
    @Inject
    private Environment environment;


    public WSResponse get(String endpoint) {
        endpoint = endpoint + getQueryString();
        Logger.info("Making GET request to :" + endpoint);
        CompletionStage<WSResponse> response = getWsRequest(endpoint).get();
        return getWSResponse(response);
    }

    /*
    Because of nginx configuration, if front app is running out side of nginx server redirection is not working.
    So failing to get scheduled users of a device. To fix this issue if app is running in dev(out side of nginx) mode handling
    redirection programmatically by changing URL to redirect.
    TODO : Remove this if block code of handling manually redirected url
    */
    public WSResponse get(String endpoint, boolean isManuallyRedirectedURL) {
        Logger.info("Making GET request to :" + endpoint);
        CompletionStage<WSResponse> response = getWsRequest(endpoint).get();
        return getWSResponse(response);
    }

    public WSResponse put(String endpoint, JsonNode json) {
        Logger.info("Making PUT request to :" + endpoint);
        return getWSResponse(getWsRequest(endpoint).put(json));
    }

    public WSResponse delete(String endpoint) {
        endpoint = endpoint + getQueryString();
        Logger.info("Making DELETE request to :" + endpoint);
        return getWSResponse(getWsRequest(endpoint).delete());
    }

    private WSResponse getWSResponse(CompletionStage<WSResponse> res) {
        WSResponse wsResponse = null;
        try {
            wsResponse = res.toCompletableFuture().get();
            /*
               Because of nginx configuration, if front app is running out side of nginx server redirection is not working.
               So failing to get scheduled users of a device. To fix this issue if app is running in dev(out side of nginx) mode handling
               redirection programmatically by changing URL to redirect.
               TODO : Remove this if block code for DEV mode
            */
/*            if (environment.isDev()) {
                if (wsResponse.getStatus() == 302 || wsResponse.getStatus() == 303) {
                    Map headers = wsResponse.getAllHeaders();
                    String url = headers.get("Location").toString();
                    if (url.contains("gateway-service")) {
                        url = url.replace("[", "").replace("]", "").replace("http://gateway-service:9002",
                                appConfig.getGatewayServiceScheme() + "://" +
                                        appConfig.getGatewayServiceHost());
                        Logger.info("redirecting to the new url : " + url + " " + request().method());
                    }
                    return get(url, true);
                }
            }*/
        } catch (InterruptedException | ExecutionException e) {
            Logger.error("Error while making request", e);
            e.printStackTrace();
            wsResponse = null;
        }
        return wsResponse;
    }

    public WSResponse post(String endpoint, JsonNode body, Map<String, String> headers) {
        Logger.info("Making POST request to :" + endpoint);
        WSRequest request = getWsRequest(endpoint);
        return getWSResponse(request.post(body));
    }

    private WSRequest setHeaders(WSRequest request) {
        Map<String, String[]> headers = Http.Context.current().request().headers();

        for (Map.Entry<String, String[]> entry : headers.entrySet()) {
            for (String value : entry.getValue()) {
                request.setHeader(entry.getKey(), value);
            }
        }
        return request;
    }


    private WSRequest getWsRequest(String url) {
        WSRequest wsRequest = wsClient.url(url);
         /*
            Because of nginx configuration, if front app is running out side of nginx server redirection is not working.
            So failing to get scheduled users of a device. To fix this issue if app is running in dev(out side of nginx) mode handling
            redirection programmatically by changing URL to redirect.
            TODO : Remove this if block code for DEV mode
         */
        if (environment.isDev()) {
            wsRequest.setFollowRedirects(false);
        }
        setHeaders(wsRequest);
        return wsRequest;
    }

    private String getQueryString() {
        String requestPath = Http.Context.current().request().path();
        String requestUri = Http.Context.current().request().uri();
        String queryString = requestUri.replace(requestPath, "");
        Logger.debug("Query string : " + queryString);

        return queryString;
    }
}
