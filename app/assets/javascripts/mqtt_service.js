(function() {
    "use strict";
    angular.module('myApp').service('MqttClient', ['$rootScope', '$timeout', '$interval', '$q', 'AppConfigService',
        function($rootScope, $timeout, $interval, $q, AppConfigService) {

            var mqttClient;
            var event_callbacks_map = {};
            var isConnecting = false;
            var retryCount = 0;
            var maxRetryCount;

            this.connect = function() {
                return $q(function(resolve, reject) {
                    if (mqttClient === undefined && isConnecting === false) {
                        isConnecting = true;
                        AppConfigService.getMqttHostConfig().then(function(config) {
                            isConnecting = false;
                            var options = {
                                clientId: "frontend-" + Math.random().toString(16).substr(2, 8),
                                port: config.mqttPort,
                                path: "/pubsub",
                                host: config.mqttHost,
                                protocol: config.mqttProtocol,
                                //                                reconnectPeriod: config.reconnectPeriod
                                username: "abc",
                                password: "123"
                            };

                            maxRetryCount = config.retryCount;
                            mqttClient = mqtt.connect(options);

                            // called when the client connects
                            mqttClient.on('connect', function() {
                                // Once a connection has been made, make a subscription and send a message.
                                console.log("Successfully connected to MQTT");
                                retryCount = 0;
                                $rootScope.$apply($rootScope.$broadcast("mqttStatus", true));
                                $rootScope.$apply(function() {
                                    angular.forEach(event_callbacks_map, function(value, key) {
                                        mqttClient.subscribe(key);
                                    });
                                });
                                resolve(mqttClient);
                            });

                            // called when the client loses its connection
                            mqttClient.on('close', function() {
                                $rootScope.$broadcast("mqttStatus", false);
                                $rootScope.$apply($rootScope.$broadcast("mqttStatus", false));
                            });

                            // called when a message arrives
                            mqttClient.on('message', function(topic, message) {
                                $rootScope.$apply(function() {
                                    console.log("Received event :" + topic + " with message : " + message);
                                    var plusCallBacks = [];
                                    // Following code is for the case where there is a + in mqtt topic
                                    angular.forEach(event_callbacks_map, function(value, key) {
                                        if (key.indexOf("+") !== -1) {
                                            var pattern = key.replace("+", ".*").replace("/", "\\/");
                                            pattern = new RegExp(pattern);
                                            if (pattern.test(topic)) {
                                                angular.forEach(value, function(v) {
                                                    plusCallBacks.push(v);
                                                });
                                            }
                                        }
                                    });

                                    var specificCallbacks = event_callbacks_map[topic] || [];
                                    var baseTopic = topic.substring(0, topic.lastIndexOf("/") + 1) + "#";
                                    var baseCallbacks = event_callbacks_map[baseTopic] || [];
                                    var callbackspre = specificCallbacks.concat(baseCallbacks);
                                    var callbacks = callbackspre.concat(plusCallBacks);
                                    angular.forEach(callbacks, function(obj) {
                                        angular.forEach(obj, function(value) {
                                            value(message, topic);
                                        });
                                    });
                                });
                            });

                            mqttClient.on('reconnect', function() {
                                retryCount++;
                                if (retryCount > maxRetryCount) {
                                    mqttClient.end(true, function() {
                                        console.log("MQTT client has been disconnected");
                                    });
                                } else {
                                    console.log("Reconnecting to MQTT");
                                }
                            });

                            mqttClient.on('offline', function() {
                                console.log("MQTT went offline");
                            });

                            mqttClient.on('error', function(error) {
                                console.log("MQTT connect error  " + error);
                            });

                        }, function(error) {
                            console.log("failed to get mqtt host config, error : " + error);
                            isConnecting = false;
                            reject("failed to connect");
                        });
                    } else {
                        if (mqttClient !== undefined && mqttClient.connected) {
                            resolve(mqttClient);
                        } else {
                            var stopTimer = $interval(function() {
                                if (mqttClient !== undefined && mqttClient.connected) {
                                    $interval.cancel(stopTimer);
                                    resolve(mqttClient);
                                }
                            }, 1000);
                        }
                    }

                });

            };

            this.mapCallbackToEvent = function(topic, ctrl, callback) {
                console.log("Adding call back for topic : " + topic + " for controller : " + ctrl);
                var callbacks = event_callbacks_map[topic] || [];
                var keyValue = {};
                keyValue[ctrl] = callback;
                callbacks.push(keyValue);
                event_callbacks_map[topic] = callbacks;
            };

            this.removeCallbackToEvent = function(topic, ctrl) {
                console.log("Removing callback for topic : " + topic + " for controller : " + ctrl);
                var callbacks = event_callbacks_map[topic] || [];
                var copyOfCallbacks = callbacks;
                angular.forEach(callbacks, function(value, key) {
                    if (value.hasOwnProperty(ctrl)) {
                        copyOfCallbacks.splice(key, 1);
                    }
                });
                event_callbacks_map[topic] = copyOfCallbacks;
            };

            this.getCallbacksForEvent = function(topic) {
                return event_callbacks_map[topic];
            };

            this.isConnected = function() {
                return mqttClient !== undefined && mqttClient.connected;
            };

            this.disconnect = function() {
                if (mqttClient) {
                    mqttClient.end(true, function() {
                        console.log("MQTT client has been disconnected");
                    });
                    event_callbacks_map = {};
                    mqttClient = undefined;
                    isConnecting = false;
                    retryCount = 0;
                }
            };

        }
    ]).service('MqttEventHandler', ['MqttClient', function(MqttClient) {

        this.subscribe = function(topic, ctrl, callback) {

            var handler = {};

            var onSuccessfulSubscription = function(err) {
                if (err === null) {
                    console.log("Successfully subscribed to " + topic);
                    //                    MqttClient.mapCallbackToEvent(topic, ctrl, callback);
                } else {
                    //As mapping event callbacks before subscription, remove callbacks if it failed to subscribe
                    MqttClient.removeCallbackToEvent(topic, ctrl);
                    console.log("failed to subscribe topic : " + topic + ", with error : " + err);
                }
            };

            var onSuccessfulUnSubscription = function() {
                console.log("Unsubscribed to " + topic);
            };

            handler.subscribe = function() {
                console.log("subscribing to : " + topic);
                // Mapping callbacks to the topic even before subscribing, as retained messages are received even before call of onSuccessfulSubscription()
                // So remove callbacks if subscription fails
                MqttClient.mapCallbackToEvent(topic, ctrl, callback);
                MqttClient.connect().then(function(mqttClient) {
                    mqttClient.subscribe(topic, onSuccessfulSubscription);
                });

            };

            handler.unsubscribe = function() {
                MqttClient.removeCallbackToEvent(topic, ctrl);

                var callbacks = MqttClient.getCallbacksForEvent(topic);
                // Unsubscribing permanently as we dont have any registered callbacks
                if (callbacks.length === 0) {
                    //MgttService.unsubscribe(topic);
                    MqttClient.connect().then(function(mqttClient) {
                        mqttClient.unsubscribe(topic, onSuccessfulUnSubscription);
                    });
                }
            };

            handler.subscribe();
            return handler;
        };

        //to publish msg on a particular topic
        this.publish = function(topic, message, options, callback) {
            console.log("Publishing to topic " + topic + " with message - " + message + " with options -" + JSON.stringify(options));
            MqttClient.connect().then(function(mqttClient) {
                mqttClient.publish(topic, message, options, callback);
            });
        };

    }]);
})();