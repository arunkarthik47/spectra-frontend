var main = angular.module('app.main', ['ngMaterial']);

main.config(['$stateProvider', '$locationProvider', 'States',
    function($stateProvider, $locationProvider, States) {
        // console.log(States);
        $stateProvider
            .state(States.APP.ROOT, {
                url: '/app',
                controller: 'mainCtrl',
                templateUrl: '/assets/views/main.html',
            })
            .state(States.APP.DEVICES, {
                url: "/devices",
                controller: 'devicesCtrl',
                templateUrl: "assets/views/devices.html"
            })
            .state(States.APP.DETAILS, {
                url: "/macId/:macId",
                controller: 'deviceDetailsCtrl',
                templateUrl: "assets/views/device_details.html",
                params: {
                    device: null,
                    macId: null
                }
            })
            .state(States.APP.VIEW3, {
                url: "/view3",
                controller: 'view3Ctrl',
                templateUrl: "assets/views/view3.html"
            });
        $locationProvider.html5Mode(true);
    }
]);

main.config(['$mdThemingProvider', function($mdThemingProvider) {

    var greyMap = $mdThemingProvider.extendPalette('grey', {
        '800': '#414042',
        'contrastDefaultColor': 'light'
    });
    $mdThemingProvider.definePalette('spectraGrey', greyMap);

    $mdThemingProvider
        .theme('default')
        .primaryPalette('spectraGrey', {
            'default': '500'
        })
        .accentPalette('blue', {
            'default': '500'
        })
        .warnPalette('red', {
            'default': '500'
        });

    $mdThemingProvider.alwaysWatchTheme(true);
    $mdThemingProvider.setDefaultTheme('default');
}]);