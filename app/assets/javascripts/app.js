var app = angular.module("myApp", ['ngMaterial', 'app.states', 'ui.router', 'app.login', 'app.main', 'nsPopover', 'md.data.table', 'ngFileUpload']);

app.config(['$stateProvider', '$locationProvider', 'States',
    function($stateProvider, $locationProvider, States) {
        $stateProvider
            /* Specifying that for the root state that the corresponding controller is AppCtrl (which redirects this state to fibosScreen state)
             */
            .state(States.ROOT, {
                url: '/',
                controller: 'AppCtrl'
            });
    }
]);