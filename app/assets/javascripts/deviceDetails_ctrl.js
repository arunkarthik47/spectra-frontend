angular.module('myApp').controller('deviceDetailsCtrl', ['$scope', '$mdToast', '$stateParams', '$http', 'notificationService', 'MqttEventHandler', function($scope, $mdToast, $stateParams, $http, notificationService, MqttEventHandler) {

    var ctrlName = "devicesController";
    var mqttEventHandler;

    $scope.showTestResult = true;

    $scope.configurations = [{
        label: "Configuration 1",
        index: 0
    }, {
        label: "Configuration 2",
        index: 1
    }, {
        label: "Configuration 3",
        index: 2
    }, {
        label: "Configuration 4",
        index: 3
    }, {
        label: "Configuration 5",
        index: 4
    }, {
        label: "Configuration 6",
        index: 5
    }, {
        label: "Configuration 7",
        index: 6
    }, {
        label: "Configuration 8",
        index: 7
    }];

    $scope.vdds = ["5.0", "3.3"];
    $scope.thresholds = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"];


    $scope.selectConfigInit = function() {
        angular.forEach($scope.device.configurations, function(value, index) {
            if (value.active === true) {
                $scope.selectedConfig = $scope.device.configurations[index];
            }
        });

    };
    $scope.getDeviceDetails = function() {
        $http({
            method: "GET",
            url: "/devices"
        }).then(function mySuccess(response) {
            $scope.devices = response.data;
            angular.forEach($scope.devices, function(value) {
                if (value.macId === $stateParams.macId) {
                    $scope.device = value;
                    $scope.deviceSaved = angular.copy(value);
                    $scope.selectConfigInit();
                }
            });
        }, function myError() {
            console.log('error');
        });
    };

    if (!$stateParams.device) {
        $scope.getDeviceDetails();
    } else {
        $scope.device = $stateParams.device;
        $scope.deviceSaved = angular.copy($stateParams.device);
        $scope.selectConfigInit();
    }
    console.log($stateParams);

    $scope.changeConfig = function(config) {
        //        $scope.index = config.index;
        angular.forEach($scope.device.configurations, function(value, index) {
            if (value.configurationName === config.configurationName) {
                $scope.device.configurations[index].active = true;
            } else {
                $scope.device.configurations[index].active = false;
            }
        });
    };

    $scope.save = function() {
        var data = $scope.device;

        $http.put('devices/frontend', data)
            .success(function() {
                notificationService.showToast("Details updated successfully");
                $scope.deviceSaved = data;
            })
            .error(function() {
                notificationService.showToast("Error, please try again");
            });

    };

    $scope.startTest = function() {
        var activeConfiguration = {};

        angular.forEach($scope.deviceSaved.configurations, function(value) {
            if (value.active === true) {
                activeConfiguration = value;
            }
        });
        var data = {
            macId: $scope.deviceSaved.macId,
            configuration: activeConfiguration,
            type: "START"
        };

        $http.post('devices/test', data)
            .success(function() {
                notificationService.showToast("Details updated successfully");
            })
            .error(function() {
                notificationService.showToast("Error, please try again");
            });

    };

    $scope.stopTest = function() {
        var activeConfiguration = {};

        angular.forEach($scope.deviceSaved.configurations, function(value) {
            if (value.active === true) {
                activeConfiguration = value;
            }
        });
        var data = {
            macId: $scope.deviceSaved.macId,
            configuration: activeConfiguration,
            type: "STOP"
        };

        $http.post('devices/test', data)
            .success(function() {
                notificationService.showToast("Command issued successfully");
            })
            .error(function() {
                notificationService.showToast("Error, please try again");
            });

    };

    $scope.reset = function() {
        var activeConfiguration = {};
        $scope.showTestResult = false;
        angular.forEach($scope.deviceSaved.configurations, function(value) {
            if (value.active === true) {
                activeConfiguration = value;
            }
        });
        var data = {
            macId: $scope.deviceSaved.macId,
            configuration: activeConfiguration,
            type: "RESET"
        };
        $http.post('devices/test', data)
            .success(function() {
                notificationService.showToast("Command issued successfully");
                $scope.showTestResult = false;
            })
            .error(function() {
                notificationService.showToast("Error, please try again");
            });

    };

    $scope.hasModuleFailed = function(modNum) {
        if ($scope.device.latestTestDetails && $scope.device.latestTestDetails.testResult && $scope.device.latestTestDetails.testResult.moduleResults[modNum]) {
            return $scope.device.latestTestDetails.testResult.moduleResults[modNum] === 'FAIL' && $scope.showTestResult;
        } else return false;
    };

    $scope.getAddress = function(module, chip) {
        //        if ($scope.device.latestTestDetails && $scope.device.latestTestDetails.testResult && $scope.device.latestTestDetails.testResult.detailedResults && $scope.device.latestTestDetails.testResult.detailedResults.modules && $scope.device.latestTestDetails.testResult.detailedResults.modules[module] && $scope.device.latestTestDetails.testResult.detailedResults.modules[module].chips[chip]) {
        //            return $scope.device.latestTestDetails.testResult.detailedResults.modules[module].chips[chip].address;
        //        }
        //        return "";

        if ($scope.device.latestTestDetails && $scope.device.latestTestDetails.testResult && $scope.device.latestTestDetails.testResult.detailedResults) {
            var index;
            if ($scope.device.panelType === 0 && chip === 0) {
                index = 2 * module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];
            }
            if ($scope.device.panelType === 0 && chip === 1) {
                index = 2 * module + 1;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];
            }
            if ($scope.device.panelType === 1 && chip === 0) {
                index = module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];

            }
            if ($scope.device.panelType === 2 && chip === 0) {
                index = 2 * module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];
            }
            if ($scope.device.panelType === 2 && chip === 1) {
                index = 2 * module + 1;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];
            }
            if ($scope.device.panelType === 3 && chip === 0) {
                index = module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index];

            }
        }
        return "";
    };

    $scope.getCurrent = function(module, chip) {
        //        if ($scope.device.latestTestDetails && $scope.device.latestTestDetails.testResult && $scope.device.latestTestDetails.testResult.detailedResults && $scope.device.latestTestDetails.testResult.detailedResults.modules && $scope.device.latestTestDetails.testResult.detailedResults.modules[module] && $scope.device.latestTestDetails.testResult.detailedResults.modules[module].chips[chip]) {
        //            return $scope.device.latestTestDetails.testResult.detailedResults.modules[module].chips[chip].current + "µA";
        //        }
        //        return "";

        if ($scope.device.latestTestDetails && $scope.device.latestTestDetails.testResult && $scope.device.latestTestDetails.testResult.detailedResults) {
            var index;
            if ($scope.device.panelType === 0 && chip === 0) {
                index = 40 + (2 * module);
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";

            }
            if ($scope.device.panelType === 0 && chip === 1) {
                index = 40 + (2 * module + 1);
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";
            }
            if ($scope.device.panelType === 1 && chip === 0) {
                index = 20 + module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";

            }
            if ($scope.device.panelType === 2 && chip === 0) {
                index = 20 + (2 * module);
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";

            }
            if ($scope.device.panelType === 2 && chip === 1) {
                index = 20 + (2 * module + 1);
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";
            }
            if ($scope.device.panelType === 3 && chip === 0) {
                index = 10 + module;
                if ($scope.device.latestTestDetails.testResult.detailedResults[index])
                    return $scope.device.latestTestDetails.testResult.detailedResults[index] + "µA";

            }
        }
        return "";
    };

    var onEventReceived = function(message) {
        message = JSON.parse(message.toString());
        ///        $scope.device = angular.copy(message);
        if ($scope.device.macId === message.macId) {
            $scope.showTestResult = true;
            $scope.device = message;
            $scope.selectConfigInit();
        }
    };

    mqttEventHandler = MqttEventHandler.subscribe("iot-service/devices", ctrlName, onEventReceived);
    //    mqttEventHandler = MqttEventHandler.subscribe("test/123", ctrlName, onEventReceived);


}]);