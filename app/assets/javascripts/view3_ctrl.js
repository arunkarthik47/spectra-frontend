angular.module('myApp').controller('view3Ctrl', ['$scope', '$mdToast', '$mdDialog', '$mdMedia', function($scope, $mdToast, $mdDialog, $mdMedia) {

    $scope.configDialog = function() {

        $mdDialog.show({
            controller: 'dialogCtrl',
            templateUrl: 'assets/views/dialog.html',
            clickOutsideToClose: true,
            fullscreen: $mdMedia('xs')
        }).then(function() {}, function() {});
    };


    $scope.desserts = [{
        device: 123,
        macId: 12345678,
        connStatus: "CONNECTED"
    }, {
        device: 234,
        macId: 12345678,
        connStatus: "CONNECTED"
    }, {
        device: 345,
        macId: 12345678,
        connStatus: "CONNECTED"
    }, {
        device: 456,
        macId: 12345678,
        connStatus: "CONNECTED"
    }];

}]);