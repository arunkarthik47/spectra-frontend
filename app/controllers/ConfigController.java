package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import utils.AppConfig;

public class ConfigController extends Controller {

    public static final String MQTT_HOST = "mqttHost";
    public static final String MQTT_PORT = "mqttPort";
    public static final String MQTT_PROTOCOL = "mqttProtocol";
    public static final String MQTT_RETRY_COUNT = "retryCount";
    public static final String MQTT_RECONNECT_PERIOD = "reconnectPeriod";
    @Inject
    private AppConfig appConfig;

    public Result mqttHostConfig() {
        ObjectNode json = Json.newObject();
        json.put(MQTT_HOST, appConfig.getMqttHost());
        json.put(MQTT_PORT, appConfig.getMqttPort());
        json.put(MQTT_PROTOCOL, appConfig.getMqttProtocol());
        json.put(MQTT_RETRY_COUNT, appConfig.getMqttRetryCount());
        json.put(MQTT_RECONNECT_PERIOD, appConfig.getMqttReconnectPeriod());
        return ok(json);
    }

    public Result getAppDetails() {
        AppDetails appDetails = new AppDetails();
        appDetails.version = appConfig.getAppVersion();
        return ok(Json.toJson(appDetails));
    }

    public Result enableTemperatureGraphAutoRefresh() {
        return ok(appConfig.isTemperatureGraphAutoRefreshEnabled().toString());
    }

    public Result getEnvironmentDetails() {
        return ok(appConfig.getEnvironment());
    }
}
