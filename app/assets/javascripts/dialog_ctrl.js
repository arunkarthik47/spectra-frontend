angular.module('myApp').controller('dialogCtrl', ['$scope', '$mdDialog', 'device', '$http', 'notificationService', function($scope, $mdDialog, device, $http, notificationService) {

    $scope.showDetails = false;
    $scope.device = device;
    $scope.cancel = function() {
        console.log('test');
        $mdDialog.cancel();
    };

    $scope.showConfigDetails = function(index) {
        $scope.showDetails = true;
        $scope.index = index;
    };

    $scope.getActiveConfig = function() {
        angular.forEach($scope.device.configurations, function(value, index) {
            if (value.active === true)
                $scope.activeButton = index;
        });
    };
    $scope.getActiveConfig();

    $scope.save = function() {
        var data = $scope.device;

        $http.put('devices/frontend', data)
            .success(function() {
                notificationService.showToast("Details updated successfully");
                $mdDialog.cancel();
            })
            .error(function() {
                notificationService.showToast("Error, please try again");
            });

    };

    $scope.changeActiveStates = function() {
        angular.forEach($scope.device.configurations, function(value, index) {
            if (index === parseInt($scope.activeButton)) {
                $scope.device.configurations[index].active = true;
            } else {
                $scope.device.configurations[index].active = false;
            }

        });
    };

}]);