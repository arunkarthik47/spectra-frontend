var login = angular.module('app.login', ['ngMaterial']);

login.config(['$stateProvider', '$locationProvider', 'States',
    function($stateProvider, $locationProvider, States) {
        // console.log(States);
        $stateProvider
            .state(States.LOGIN, {
                url: '/login',

                controller: 'loginCtrl',
                templateUrl: '/assets/views/login.html',
            });
        $locationProvider.html5Mode(true);
    }
]);