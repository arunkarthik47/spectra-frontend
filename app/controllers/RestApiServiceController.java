package controllers;

import play.Logger;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import java.util.Map;

public class RestApiServiceController extends Controller {

    public Result getResult(WSResponse res) {
        if (res == null) {
            return internalServerError();
        }
        Logger.info("Hitting end-point : {} returned response : {}", res.getUri(), res.getStatus());
        Map<String, List<String>> responseHeaders = res.getAllHeaders();

        for(Map.Entry<String, List<String>> header : responseHeaders.entrySet()) {
            List<String> values = header.getValue();
            for(String value : values) {
                response().setHeader(header.getKey(), value);
            }
        }

        return status(res.getStatus(), res.getBodyAsStream());
    }
}
