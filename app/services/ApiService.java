package services;

public interface ApiService {
    String getApiServiceUri(String uri);
}
