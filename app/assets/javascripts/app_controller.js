angular.module('myApp').controller('AppCtrl', ['$rootScope', '$scope', 'States', '$state',
    function($rootScope, $scope, States, $state) {

        /* The app is redirected to fibosScreen state i.e States.FIBOS when the state is States.ROOT
         */

        if ($state.$current.name === States.ROOT)
            $state.go(States.LOGIN);

    }
]);